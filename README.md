# Divante Test

Junior PHP Developer code challenge

## Development Environment

* **Linux Ubuntu 18.04.5**
* **PHP 8.0.3**
* **Docker 20.10.15**
* **Docker Compose 1.29.1**

## Installation

```sh
$ git clone https://paneric@bitbucket.org/paneric/divante-test.git
$ cd divante-test
$ docker-compose up -d
$ composer install
```

## Exception remarks

Errors, warnings and exceptions are handled in the same manner to unify issues info presentation (caught as throwable and thrown as exception):

*src/Parser/ParserXml.php*
```php
<?php

/*...*/

class ParserXml extends Parser implements ParserInterface
{
    /*...*/

    /**
     * @param string $content
     * @return array
     * @throws InvalidParserException
     */
    public function parse(string $content): array
    {
        try {
            $xml = simplexml_load_string(
                $content,
                "SimpleXMLElement",
                LIBXML_NOCDATA
            );

            $data = json_decode(
                json_encode($xml, JSON_THROW_ON_ERROR),
                TRUE,
                512,
                JSON_THROW_ON_ERROR
            );

            return $data[$this->key];
        } catch (Throwable $e) {
            throw new InvalidParserException(trim($e->getMessage()));
        }
    }
}
```

*src/Parser/ParserJson.php*
```php
<?php

/*...*/

class ParserJson extends Parser implements ParserInterface
{
    /*...*/

    /**
     * @param string $content
     * @return array
     * @throws InvalidParserException
     */
    public function parse(string $content): array
    {
        try {
            $data = json_decode(
                $content,
                true,
                512,
                JSON_THROW_ON_ERROR
            );

            return $data[$this->key];
        } catch (Throwable $e) {
            throw new InvalidParserException(trim($e->getMessage()));
        }
    }
}
```

*src/Supplier/Supplier.php*
```php
<?php

/*...*/

class Supplier implements SupplierInterface
{
    /*...*/

    /**
     * @return string
     * @throws InvalidParserException
     */
    protected function getResponse(): string
    {
        try {
            return file_get_contents($this->url);
        } catch (Throwable $e) {
            throw new InvalidParserException(trim($e->getMessage()));
        }
    }
}
```



## Commands

```sh
$ php bin/console divante:supplier-sync supplier1

$ php bin/console divante:supplier-sync supplier2

$ php bin/console divante:supplier-sync supplier3
```
