<?php

declare(strict_types=1);

namespace App\Config;

class Config
{
    public const NAME_1 = 'supplier1';
    public const TYPE_1 = 'xml1';
    public const KEY_1  = 'product';
    public const URL_1  = 'http://localhost/suppliers/supplier1.xml';

    public const NAME_2 = 'supplier2';
    public const TYPE_2 = 'xml2';
    public const KEY_2  = 'item';
    public const URL_2  = 'http://localhost/suppliers/supplier2.xml';

    public const NAME_3 = 'supplier3';
    public const TYPE_3 = 'json';
    public const KEY_3  = 'list';
    public const URL_3  = 'http://localhost/suppliers/supplier3.json';
}
