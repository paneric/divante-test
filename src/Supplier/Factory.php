<?php

declare(strict_types=1);

namespace App\Supplier;

use App\Config\Config;
use App\Event\IntegrationEvents;
use App\Listener\ProductsListener;
use App\Parser\FactoryInterface as ParserFactoryInterface;
use InvalidArgumentException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Factory implements FactoryInterface
{
    protected ParserFactoryInterface $parserFactory;

    protected EventDispatcherInterface $eventDispatcher;

    public function __construct(ParserFactoryInterface $parserFactory, EventDispatcherInterface $eventDispatcher)
    {
        $this->parserFactory = $parserFactory;
        $this->eventDispatcher = $eventDispatcher;

        $this->eventDispatcher->addListener(
            IntegrationEvents::SUPPLIER_GET_PRODUCTS,
            [new ProductsListener(), 'logProducts']
        );
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getSupplier(string $supplierName): SupplierInterface
    {
        if($supplierName === Config::NAME_1) {
            return new Supplier1(
                $this->parserFactory->getParser(Config::TYPE_1),
                $this->eventDispatcher
            );
        }
        if($supplierName === Config::NAME_2) {
            return new Supplier2(
                $this->parserFactory->getParser(Config::TYPE_2),
                $this->eventDispatcher
            );
        }
        if($supplierName === Config::NAME_3) {
            return new Supplier3(
                $this->parserFactory->getParser(Config::TYPE_3),
                $this->eventDispatcher
            );
        }

        throw new InvalidArgumentException('Unknown supplier name given');
    }
}
