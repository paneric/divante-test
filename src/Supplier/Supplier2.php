<?php

declare(strict_types=1);

namespace App\Supplier;

use App\Config\Config;
use App\Parser\ParserInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class Supplier2 extends Supplier
{
    public function __construct(ParserInterface $parser, EventDispatcher $eventDispatcher)
    {
        parent::__construct($parser, $eventDispatcher);

        self::$name = Config::NAME_2;
        self::$type = Config::TYPE_2;
        $this->url  = Config::URL_2;
    }
}
