<?php

declare(strict_types=1);

namespace App\Supplier;

use App\Event\GetProductsEvent;
use App\Event\IntegrationEvents;
use App\Exception\InvalidParserException;
use App\Parser\ParserInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Throwable;

class Supplier implements SupplierInterface
{
    protected ParserInterface $parser;
    protected EventDispatcher$eventDispatcher;

    protected static string $name;
    protected static string $type;
    protected string $url;

    public function __construct(ParserInterface $parser, EventDispatcher $eventDispatcher)
    {
        $this->parser = $parser;
        $this->eventDispatcher = $eventDispatcher;
    }

    public static function getName(): string
    {
        return self::$name;
    }

    public static function getResponseType(): string
    {
        return self::$type;
    }

    /**
     * @throws InvalidParserException
     */
    public function getProducts(): array
    {
        $products = $this->parseResponse();

        $this->eventDispatcher->dispatch(
            new GetProductsEvent($products, self::getName()),
            IntegrationEvents::SUPPLIER_GET_PRODUCTS
        );

        return $products;
    }

    /**
     * @throws InvalidParserException
     */
    protected function parseResponse(): array
    {
        $content = $this->getResponse();

        return $this->parser->parse($content);
    }

    /**
     * @throws InvalidParserException
     */
    protected function getResponse(): string
    {
        try {
            return file_get_contents($this->url);
        } catch (Throwable $e) {
            throw new InvalidParserException(trim($e->getMessage()));
        }
    }
}
