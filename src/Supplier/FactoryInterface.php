<?php

declare(strict_types=1);

namespace App\Supplier;

use InvalidArgumentException;

interface FactoryInterface
{
    /**
     * @throws InvalidArgumentException
     */
    public function getSupplier(string $supplierName): SupplierInterface;
}
