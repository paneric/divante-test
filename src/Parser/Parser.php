<?php

declare(strict_types=1);

namespace App\Parser;

class Parser
{
    protected static string $type;

    protected string $key;

    public static function getType(): string
    {
        return self::$type;
    }
}
