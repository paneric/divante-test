<?php

declare(strict_types=1);

namespace App\Parser;

use App\Exception\InvalidParserException;
use Throwable;

class ParserJson extends Parser implements ParserInterface
{
    /**
     * @throws InvalidParserException
     */
    public function parse(string $content): array
    {
        try {
            $data = json_decode(
                $content,
                true,
                512,
                JSON_THROW_ON_ERROR
            );

            return $data[$this->key];
        } catch (Throwable $e) {
            throw new InvalidParserException(trim($e->getMessage()));
        }
    }
}
