<?php

declare(strict_types=1);

namespace App\Parser;

use App\Config\Config;

class Parser2 extends ParserXml
{
    public function __construct()
    {
        self::$type = Config::TYPE_2;
        $this->key  = Config::KEY_2;
    }
}
