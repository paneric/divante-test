<?php

declare(strict_types=1);

namespace App\Parser;

use App\Exception\InvalidParserException;
use Throwable;

class ParserXml extends Parser implements ParserInterface
{
    /**
     * @throws InvalidParserException
     */
    public function parse(string $content): array
    {
        try {
            $xml = simplexml_load_string(
                $content,
                "SimpleXMLElement",
                LIBXML_NOCDATA
            );

            $data = json_decode(
                json_encode($xml, JSON_THROW_ON_ERROR),
                TRUE,
                512,
                JSON_THROW_ON_ERROR
            );

            return $data[$this->key];
        } catch (Throwable $e) {
            throw new InvalidParserException(trim($e->getMessage()));
        }
    }
}
