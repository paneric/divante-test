<?php

declare(strict_types=1);

namespace App\Parser;

use App\Config\Config;

class Parser3 extends ParserJson
{
    public function __construct()
    {
        self::$type = Config::TYPE_3;
        $this->key  = Config::KEY_3;
    }
}
