<?php

declare(strict_types=1);

namespace App\Parser;

use App\Config\Config;
use InvalidArgumentException;

class Factory implements FactoryInterface
{
    /**
     * @throws InvalidArgumentException
     */
    public function getParser(string $type): ParserInterface
    {
        if($type === Config::TYPE_1) {
            return new Parser1();
        }
        if($type === Config::TYPE_2) {
            return new Parser2();
        }
        if($type === Config::TYPE_3) {
            return new Parser3();
        }

        throw new InvalidArgumentException('Unknown parser type given');
    }
}
