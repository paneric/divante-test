<?php

declare(strict_types=1);

namespace App\Parser;

use App\Config\Config;

class Parser1 extends ParserXml
{
    public function __construct()
    {
        self::$type = Config::TYPE_1;
        $this->key  = Config::KEY_1;
    }
}
