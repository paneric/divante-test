<?php

declare(strict_types=1);

namespace App\Parser;

use InvalidArgumentException;

interface FactoryInterface
{
    /**
     * @throws InvalidArgumentException
     */
    public function getParser(string $type): ParserInterface;
}
