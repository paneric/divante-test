<?php

declare(strict_types=1);

namespace App\Parser;

use App\Exception\InvalidParserException;

interface ParserInterface
{
    public static function getType(): string;

    /**
     * @throws InvalidParserException
     */
    public function parse(string $content): array;
}
